const sortTableHeaderIds = [
  "table-header-name",
  "table-header-speciality",
  "table-header-age",
  "table-header-gender",
  "table-header-nationality",
];
const sortTableParamNames = ["full_name", "course", "age", "gender", "country"];
const sortTableOrderAsc = [true, undefined, undefined, undefined, undefined];
let currentPage = 1;
var map, specialityChart, ageChart, genderChart, nationalityChart;

function createTeacherCard(user, showFavorite = true) {
  const teacherCard = document.createElement("div");
  const id = user.email.substring(0, user.email.indexOf("@")).replace(".", "-");
  teacherCard.classList.add("teacher-card");
  teacherCard.classList.add(`teacher-card-${id}`);

  const profileImageContainer = document.createElement("div");
  profileImageContainer.classList.add("teacher-profile-image");

  if (user.picture) { 
    const img = document.createElement("img");
    img.classList.add("teacher-card-photo");
    img.src = user.picture;
    img.alt = user.full_name;
    img.width = "100";
    img.height = "100";
    profileImageContainer.appendChild(img);
  } else { 
    const initialsDiv = document.createElement("div");
    initialsDiv.classList.add("teacher-card-initials");
    const initials = document.createElement("p");
    initials.textContent = getInitials(user.full_name);
    initialsDiv.appendChild(initials);
    profileImageContainer.appendChild(initialsDiv);
  }

  teacherCard.appendChild(profileImageContainer);

  const name = document.createElement("p");
  name.classList.add("teacher-card-name");
  name.textContent = user.full_name;
  teacherCard.appendChild(name);

  const specialty = document.createElement("p");
  specialty.classList.add("teacher-card-speciality");
  specialty.textContent = user.course;
  teacherCard.appendChild(specialty);

  const country = document.createElement("p");
  country.classList.add("teacher-card-country");
  country.textContent = user.country;
  teacherCard.appendChild(country);

  if (user.favorite && showFavorite) {
    const starImage = document.createElement("img");
    starImage.classList.add("teacher-star");
    starImage.src = "images/star.png";
    starImage.alt = "Star";
    starImage.width = "40";
    starImage.height = "40";
    teacherCard.insertBefore(starImage, name);
  }
  teacherCard.addEventListener("click", () => {
    const popup = document.querySelector(".popup-teacher-info");
    popup.setAttribute("open", "");
    document.querySelector(".popup-teacher-info .teacher-info-photo").src = user.picture;
    document.querySelector(".popup-teacher-info .info-name").textContent = user.full_name;
    document.querySelector(".popup-teacher-info .info-speciality").textContent = user.course;
    document.querySelector(".popup-teacher-info .info-age").textContent = user.age;
    document.querySelector(".popup-teacher-info .info-location").textContent = `${user.city}, ${user.country}`;
    document.querySelector(".popup-teacher-info .info-email").textContent = user.email;
    document.querySelector(".popup-teacher-info .info-phone").textContent = user.phone;
    document.querySelector(".popup-teacher-info .bio").textContent = user.note;
    const today = dayjs();
    const birthdate = dayjs(user.b_date);
    let nextBirthday = birthdate.set('year', today.year());
    if (nextBirthday.isBefore(today)) {
      nextBirthday = nextBirthday.add(1, 'year');
    }
    const daysUntilBirthday = nextBirthday.diff(today, 'day');
    const birthdayCountdownElement = document.querySelector(".popup-teacher-info .birthday-countdown");
    birthdayCountdownElement.textContent = `Days until birthday: ${daysUntilBirthday}`;

    if (map != undefined) {
      map.remove();
    }
    map = L.map("map").setView(
      [user.coordinates.latitude, user.coordinates.longitude],
      1
    );

    L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);

    L.marker([user.coordinates.latitude, user.coordinates.longitude])
      .addTo(map)
      .bindPopup(`${user.full_name} location`)
      .openPopup();
    const favoriteImg = document.querySelector(".favorite-img")
    favoriteImg.src = user.favorite ? "images/star.png" : "images/star-outline.png";
    
    const favoriteClick = function() {
      user.favorite = !user.favorite;
      updateUsers();
      popup.removeAttribute("open");
    }

    favoriteImg.addEventListener("click", favoriteClick, {once : true});
  });
  return teacherCard;
}

function addToTopTeachers(user) {
  const teacherGrid = document.querySelector(".teacher-grid");
  const teacherCard = createTeacherCard(user);
  teacherGrid.appendChild(teacherCard);
}

function addToTable(user) {
  const table = document.querySelector("#statistics table");
  const tr = document.createElement("tr");
  for (key of ["full_name", "course", "age", "gender", "country"]) {
    const element = document.createElement("td");
    element.textContent =
      key == "gender"
        ? `${user[key][0].toUpperCase()}${user[key].slice(1)}`
        : user[key];
    tr.appendChild(element);
  }
  table.appendChild(tr);
}

function addToFavorites(user) {
  const teacherList = document.querySelector("#favorites .teacher-list");
  const teacherCard = createTeacherCard(user, false);
  teacherList.appendChild(teacherCard);
}

function updateUsersWithInputData(users, filter, searchInput, tableOrderAsc) {
  document.querySelector(".teacher-grid").innerHTML = "";
  document.querySelector(".teacher-list").innerHTML = "";
  document.querySelectorAll("#statistics table tr:not(:first-child)").forEach((tr) => tr.remove());

  const filteredUsers = filterUsersByParams(users, filter).filter((user) => {
    return user.full_name.toLowerCase().includes(searchInput.toLowerCase()) ||
      user.note.toLowerCase().includes(searchInput.toLowerCase()) ||
      user.age == searchInput;
  });
  filteredUsers.forEach((user) => {
    addToTopTeachers(user);
  });

  users.forEach((user) => {
    if (user.favorite) addToFavorites(user);
  });

  const sortIndex = tableOrderAsc.findIndex(
    (orderAsc) => orderAsc != undefined
  );
  sliceStart = (currentPage - 1) * 10;
  sliceEnd = sliceStart + 10;

  if (sortIndex != -1) {
    const sortParam = sortTableParamNames[sortIndex];
    const sortOrder = sortTableOrderAsc[sortIndex] ? "asc" : "desc";
    sortUsersByParams(filteredUsers, sortParam, sortOrder).forEach(addToTable);
  } else {
    filteredUsers.slice(sliceStart, sliceEnd).forEach(addToTable);
  }
}

function initSpecialityPieChart() {
  const specialtyCounts = new Array(courses.length).fill(0);

  users.forEach((user) => {
    const specialtyIndex = courses.indexOf(user.course);
    if (specialtyIndex !== -1) {
      specialtyCounts[specialtyIndex]++;
    }
  });

  const specialityChartElement = document.getElementById("chart-speciality");
  if (specialityChart != undefined) {
    specialityChart.destroy()
  }
  specialityChart = new Chart(specialityChartElement, {
    type: "pie",
    data: {
      labels: courses,
      datasets: [
        {
          label: "# of teachers ",
          data: specialtyCounts,
          borderWidth: 1,
        },
      ],
    },
  });
}

function initAgePieChart() {
  const ageLabels = ["18-30", "31-40", "41-50", "51-60", "60+"];
  const ageCounts = new Array(ageLabels.length).fill(0);

  users.forEach((user) => {
    if (user.age < 31) ageCounts[0]++;
    else if (user.age < 41) ageCounts[1]++;
    else if (user.age < 51) ageCounts[2]++;
    else if (user.age < 61) ageCounts[3]++;
    else ageCounts[4]++;
  });

  const ageChartElement = document.getElementById("chart-age");
  if (ageChart != undefined) {
    ageChart.destroy()
  }
  ageChart = new Chart(ageChartElement, {
    type: "pie",
    data: {
      labels: ageLabels,
      datasets: [
        {
          label: "# of teachers ",
          data: ageCounts,
          borderWidth: 1,
        },
      ],
    },
  });
}

function initGenderPieChart() {
  const gendersData = [0, 0];

  users.forEach((user) => {
    if (user.gender.toLowerCase() == "male") gendersData[0]++;
    else if (user.gender.toLowerCase() == "female") gendersData[1]++;
  });

  const genderChartElement = document.getElementById("chart-gender");
  if (genderChart != undefined) {
    genderChart.destroy()
  }
  genderChart = new Chart(genderChartElement, {
    type: "pie",
    data: {
      labels: ["Male", "Female"],
      datasets: [
        {
          label: "# of teachers ",
          data: gendersData,
          borderWidth: 1,
        },
      ],
    },
  });
}

function initNationalityChart() {
  const countries = [];
  const countriesData = [];

  users.forEach((user) => {
    const country = user.country;
    if (countries.includes(country)) {
      countriesData[countries.indexOf(country)]++;
    } else {
      countries.push(country);
      countriesData.push(0)
    }
  });

  const nationalityChartElement = document.getElementById("chart-nationality");
  if (nationalityChart != undefined) {
    nationalityChart.destroy()
  }
  nationalityChart = new Chart(nationalityChartElement, {
    type: "pie",
    data: {
      labels: countries,
      datasets: [
        {
          label: "# of teachers ",
          data: countriesData,
          borderWidth: 1,
        },
      ],
    },
  });
}

function initPieCharts() {
  initSpecialityPieChart();
  initAgePieChart();
  initGenderPieChart();
  initNationalityChart();
}


function updateUsers() {
  initPagination();
  initPieCharts();
  const ageSelector = document.getElementById("age");
  const regionSelector = document.getElementById("region");
  const sexSelector = document.getElementById("sex");
  const onlyPhotoSelector = document.getElementById("only-photo");
  const onlyFavoritesSelector = document.getElementById("only-favorites");
  const ageValue = ageSelector.options[ageSelector.selectedIndex].value;
  const regionValue = regionSelector.options[regionSelector.selectedIndex].value;
  const sexValue = sexSelector.options[sexSelector.selectedIndex].value;
  const onlyPhotoValue = onlyPhotoSelector.checked;
  const onlyFavoritesValue = onlyFavoritesSelector.checked;
  const filters = {
    age: ageValue != "all" ? ageValue : undefined,
    region: regionValue != "all" ? regionValue : undefined,
    gender: sexValue != "all" ? sexValue : undefined,
    hasPicture: onlyPhotoValue === true ? true : undefined,
    favorite: onlyFavoritesValue === true ? true : undefined,
  };
  const searchInputValue = document.getElementById("search-input").value
  updateUsersWithInputData(users, filters, searchInputValue, sortTableOrderAsc);
}

let users = formatUsers(randomUserMock, additionalUsers).filter(isValid);
getUsers().then((apiUsers) => {
  users = formatUsers(apiUsers, []);
  updateUsers();
});

updateUsers(); 
document.getElementById("search-input").addEventListener("input", updateUsers);
["age", "region", "sex", "only-photo", "only-favorites"].forEach((id) => {
  document.getElementById(id).addEventListener("change", updateUsers);
});


for (let i = 0; i < sortTableHeaderIds.length; i++) {
  document
    .getElementById(sortTableHeaderIds[i])
    .addEventListener("click", () => {
      if (sortTableOrderAsc[i] != undefined) {
        sortTableOrderAsc[i] = !sortTableOrderAsc[i];
        document.querySelector(
          `#${sortTableHeaderIds[i]} .arrow-th`
        ).style.transform = `${
          sortTableOrderAsc[i] ? "rotate(0deg)" : "rotate(180deg)"
        }`;
      } else {
        for (let j = 0; j < sortTableOrderAsc.length; j++) {
          sortTableOrderAsc[j] = undefined;
          document.querySelector(
            `#${sortTableHeaderIds[j]} .arrow-th`
          ).style.transform = "rotate(0deg)";
        }
        sortTableOrderAsc[i] = false;
        document.querySelector(
          `#${sortTableHeaderIds[i]} .arrow-th`
        ).style.transform = "rotate(180deg)";
      }
      updateUsers();
    });
}


const addTeacherPopup = document.querySelector(".popup-add-teacher");
const addTeacherBtns = document.querySelectorAll(".add-teacher-btn");
addTeacherBtns.forEach((btn) => {
  btn.addEventListener("click", () => {
    addTeacherPopup.setAttribute("open", "");
  });
});
const sumbitBtn = document.querySelector(".add-btn");
sumbitBtn.addEventListener("click", (event) => {
  event.preventDefault();
  const bDate = document.querySelector("#date-of-birth-input").value
  const teacher = {
    full_name: document.querySelector("#name-input").value,
    course: document.querySelector("#speciality-input").value,
    country: document.querySelector("#country-input").value,
    city: document.querySelector("#city-input").value,
    state: document.querySelector("#city-input").value,
    email: document.querySelector("#email-input").value,
    phone: document.querySelector("#phone-input").value,
    b_date: bDate,
    age: getAge(bDate),
    gender: document.querySelector('input[name="sex"]:checked').value.toLocaleLowerCase(),
    bg_color: document.querySelector("#background-color-input").value,
    note: document.querySelector("#notes-input").value,
  }
  
    users.push(teacher);
    postNewTeacher(teacher)
    updateUsers();
    addTeacherPopup.removeAttribute("open");
});
function postNewTeacher(data){
  fetch('http://localhost:3000/users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((result) => {
      console.log('Дані були успішно відправлені на сервер', result);
    })
    .catch((error) => {
      console.error('Помилка під час відправки POST-запиту', error);
    });
}


const arrowLeft = document.querySelector(".arrow.left");
arrowLeft.addEventListener("click", () => {
  document.querySelector(".teacher-list").scrollLeft -= 200;
});
const arrowRight = document.querySelector(".arrow.right");
arrowRight.addEventListener("click", () => {
  document.querySelector(".teacher-list").scrollLeft += 200;
});

function initPagination() {
  const tableNav = document.querySelector("#statictics-table-nav");
  const lastNumber = Math.ceil(users.length / 10);
  if (currentPage == 1) {
    tableNav.innerHTML = `
      <div class="table-page active-page">${currentPage}</div>
      <div class="table-page">${currentPage + 1}</div>
      <div class="table-page">${currentPage + 2}</div>
      <div>...</div>
      <div class="table-page">Last</div>
    `;
  } else if (currentPage == lastNumber) {
    tableNav.innerHTML = `
      <div class="table-page">${currentPage - 2}</div>
      <div class="table-page">${currentPage - 1}</div>
      <div class="table-page active-page">${currentPage}</div>
      <div>...</div>
      <div class="table-page">Last</div>
    `;
  } else {
    tableNav.innerHTML = `
      <div class="table-page">${currentPage - 1}</div>
      <div class="table-page active-page">${currentPage}</div>
      <div class="table-page">${currentPage + 1}</div>
      <div>...</div>
      <div class="table-page">Last</div>
    `;
  }

  const pages = document.querySelectorAll(".table-page");
  pages.forEach((page) => {
    page.addEventListener("click", () => {
      const text = page.innerText;
      const number = text == "Last" ? lastNumber : parseInt(text);
      currentPage = number;
      updateUsers();
    });
  });
}