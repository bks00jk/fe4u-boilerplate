function httpGet(url) {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open("GET", url, true);
      xhr.onload = function () {
        if (this.status == 200) {
          resolve(this.response);
        } else {
          let error = new Error(this.statusText);
          error.code = this.status;
          reject(error);
        }
      };
  
      xhr.onerror = function () {
        reject(new Error("Network Error"));
      };
  
      xhr.send();
    });
  }
  
  async function getUsers() {
    const response = await httpGet(`https://randomuser.me/api/?results=50`);
    const results = JSON.parse(response).results;
    return results;
  }
  